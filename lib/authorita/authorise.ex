defmodule Authorita.Authorise do
  import Plug.Conn

  def init(options) do
    options
  end

  def call(conn, opts) do
    role = Plug.Conn.get_session(conn, :current_user_role)
    opts = {Keyword.get(opts, :roles, []), Keyword.get(opts, :redirect, false)}
    check_role(conn, opts, role)
  end

  defp check_role(conn, {roles, redirect}, role) do
    if role in roles, do: conn, else: handle_error(conn, role, "No permission", redirect)
  end

  defp handle_error(conn, _role, message, false) do
    send_resp(conn, 403, Poison.encode!(%{"error" => message})) |> halt()
  end

  defp handle_error(conn, role, message, true) do
    redirect_pages = Application.get_env(:authorita, :redirect_pages)
    conn
      |> Phoenix.Controller.redirect(to: redirect_pages[role])
      |> halt
  end

  @doc """
  """
  defp redirect_to(%Plug.Conn{resp_headers: resp_headers} = conn, address, message) do
    new_headers = [{"content-type", "text/html; charset=utf-8"}, {"location", address}]
    %{conn | resp_headers: new_headers ++ resp_headers}
    |> print_message(message)
    |> send_resp(302, "")
    |> halt()
  end

  defp print_message(conn, message) do
    if Map.get(conn.private, :phoenix_flash) do
      put_private(conn, :phoenix_flash, message)
    else
      conn
    end
  end
end
