defmodule AuthoritaTest do
  use ExUnit.Case
  use Plug.Test

  alias Authorita.Authorise
  alias Authorita.User

  @authorise_opts roles: [:admin]
  @opts Authorita.Authorise.init(@authorise_opts)

  @default_opts [
    store: :cookie,
    key: "foobar",
    encryption_salt: "encrypted cookie salt",
    signing_salt: "signing salt"
  ]

  @secret String.duplicate("abcdef0123456789", 8)
  @signing_opts Plug.Session.init(Keyword.put(@default_opts, :encrypt, false))

  doctest Authorita

  defp sign_conn(conn, secret \\ @secret) do
    put_in(conn.secret_key_base, secret)
    |> Plug.Session.call(@signing_opts)
    |> fetch_session
  end

  test "Should return error if user does not have roles" do
    user = %User{id: 1, username: "damian", role: :anonymous}

    conn = conn(:get, "/hello")
           |> sign_conn
           |> put_session(:current_user_role, user.role)

    conn = Authorise.call(conn, @opts)

    assert "{\"error\":\"No permission\"}" === conn.resp_body
  end

  test "Should handle user with no roles gracefully" do
    user = %User{id: 1, username: "damian"}

    conn = conn(:get, "/hello")
           |> sign_conn
           |> put_session(:current_user_role, user.role)

    conn = Authorise.call(conn, @opts)

    assert "{\"error\":\"No permission\"}" === conn.resp_body
  end

  test "Should return conn if authorised" do
    user = %User{id: 1, username: "damian", role: :admin}

    conn = conn(:get, "/hello")
           |> sign_conn
           |> put_session(:current_user_role, user.role)

    assert conn == Authorise.call(conn, @opts)
  end

  test "should redirect to specified location" do
    user = %User{id: 1, username: "damian"}

    conn = conn(:get, "/hello")
           |> sign_conn
           |> put_session(:current_user_role, user.role)

    opts = Authorita.Authorise.init([roles: ["admin"], redirect: true])

    conn = Authorise.call(conn, opts)

    assert conn.status == 302
  end
end
