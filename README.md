# Authorita

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add authorita to your list of dependencies in `mix.exs`:

        def deps do
          [{:authorita, "~> 0.0.1"}]
        end

  2. Ensure authorita is started before your application:

        def application do
          [applications: [:authorita]]
        end
